import org.springframework.boot.gradle.tasks.bundling.BootJar

plugins {
    `java-library`
    id("com.google.protobuf") version "0.9.3"
    id("com.vigil.trading.common-dependencies") version "1.0.1"
}

version = "2.2.19"

sourceSets {
    main {
        proto {
            srcDir("proto/src")
        }
    }
}

protobuf {
    plugins {
        create("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:1.56.1"
        }
    }
    generateProtoTasks {
        all().forEach {
            it.plugins {
                create("grpc")
            }
        }
    }
}

tasks.named<BootJar>("bootJar") {
    isEnabled = false
}

dependencies {
    api("org.springframework.boot:spring-boot-starter-web")
    api("org.springframework.boot:spring-boot-starter-actuator")
    api("com.freemanan:grpc-client-boot-starter:3.1.0")

    implementation("org.springframework.cloud:spring-cloud-starter-netflix-eureka-client")
    implementation("org.springframework.cloud:spring-cloud-starter-openfeign")
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
    implementation("ch.obermuhlner:big-math:2.3.2")
}
