#!/bin/bash

PB_REL="https://github.com/protocolbuffers/protobuf/releases"
curl -LO $PB_REL/download/v23.4/protoc-23.4-linux-x86_64.zip
unzip protoc-23.4-linux-x86_64.zip -d "$HOME/.local"
export PATH="$PATH:$HOME/.local/bin"
