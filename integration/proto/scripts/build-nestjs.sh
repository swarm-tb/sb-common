#!/bin/bash

protoc --plugin="$(npm root)/.bin/protoc-gen-ts_proto" \
 --ts_proto_out=dist/nestjs \
 --ts_proto_opt=nestJs=true \
 --ts_proto_opt=esModuleInterop=true \
 -I=src/ src/*.proto
