#!/bin/bash

npm install && npm run build
npm config set "@swarm-tb:registry" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/"
npm config set "//${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken" "${CI_JOB_TOKEN}"
