package com.vigil.trading.integration.records.range;

import java.time.Instant;

public record SbTimeRange(
    Instant fromTimestamp,
    Instant toTimestamp
) {

  public boolean isWithinRange(final Instant timestamp) {
    return fromTimestamp.compareTo(timestamp) <= 0
        && timestamp.compareTo(toTimestamp) <= 0;
  }

  public static SbTimeRange ofSanitized(final Instant fromTimestamp, final Instant toTimestamp) {
    return new SbTimeRange(
        fromTimestamp,
        getCurrentTimeIfNull(toTimestamp)
    );
  }

  public static SbTimeRange ofSanitized(final SbTimeRange timeRange) {
    return ofSanitized(timeRange.fromTimestamp(), getCurrentTimeIfNull(timeRange.toTimestamp()));
  }

  private static Instant getCurrentTimeIfNull(final Instant timestamp) {
    return timestamp == null
        ? Instant.now()
        : timestamp;
  }
}
