package com.vigil.trading.integration.utility;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public final class SbIntervalUtils {

  private static final Map<Duration, String> DURATION_STRING_MAP;

  static {
    DURATION_STRING_MAP = new HashMap<>();
    DURATION_STRING_MAP.put(Duration.ofMinutes(1), "1m");
    DURATION_STRING_MAP.put(Duration.ofMinutes(3), "3m");
    DURATION_STRING_MAP.put(Duration.ofMinutes(5), "5m");
    DURATION_STRING_MAP.put(Duration.ofMinutes(15), "15m");
    DURATION_STRING_MAP.put(Duration.ofMinutes(30), "30m");
    DURATION_STRING_MAP.put(Duration.ofHours(1), "1h");
    DURATION_STRING_MAP.put(Duration.ofHours(2), "2h");
  }

  public static String getInterval(final Duration duration) {
    return DURATION_STRING_MAP.get(duration);
  }

  private SbIntervalUtils() {
    throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
  }
}
