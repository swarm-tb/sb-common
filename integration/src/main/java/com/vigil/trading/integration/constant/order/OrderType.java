package com.vigil.trading.integration.constant.order;

public enum OrderType {
  MARKET,
  LIMIT
}
