package com.vigil.trading.integration.dto.history

import com.vigil.trading.integration.constant.order.OrderSide
import java.math.BigDecimal
import java.time.Instant

@JvmRecord
data class SbPositionHistoryRecord(
    val timestamp: Instant,
    val side: OrderSide,
    val orderSize: BigDecimal,
    val price: BigDecimal,
    val totalSize: BigDecimal,
    val totalValueUsed: BigDecimal,
    val pNl: BigDecimal,
    val pNlDelta: BigDecimal
)
