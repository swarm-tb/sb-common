package com.vigil.trading.integration.records;

import java.math.BigDecimal;

public record MinMax(
    BigDecimal min,
    BigDecimal max
) {
}
