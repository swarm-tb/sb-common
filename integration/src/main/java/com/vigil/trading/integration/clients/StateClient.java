package com.vigil.trading.integration.clients;

import com.vigil.trading.integration.constant.Endpoint;
import com.vigil.trading.integration.constant.Endpoint.Path;
import com.vigil.trading.integration.constant.Endpoint.PathVariableName;
import com.vigil.trading.integration.constant.SbAccount;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@FeignClient(name = "${service-name.state-service}", path = Endpoint.STATE)
public interface StateClient {

  @PutMapping(Path.REFRESH_STATE)
  void refreshLastChangeTimestamp(
      @PathVariable(PathVariableName.ACCOUNT) SbAccount account,
      @PathVariable(PathVariableName.SYMBOL) String symbol
  );
}
