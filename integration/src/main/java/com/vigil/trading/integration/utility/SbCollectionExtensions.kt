package com.vigil.trading.integration.utility

import com.vigil.trading.integration.records.MinMax
import java.math.BigDecimal

fun <E> List<E>.getMinMax(getValue: (E) -> BigDecimal): MinMax {
    val firstValue = getValue(this[0])
    return this.fold(MinMax(firstValue, firstValue)) { acc, element ->
        val currentValue = getValue(element)
        MinMax(
            if (currentValue < acc.min) currentValue else acc.min,
            if (currentValue > acc.max) currentValue else acc.max
        )
    }
}
