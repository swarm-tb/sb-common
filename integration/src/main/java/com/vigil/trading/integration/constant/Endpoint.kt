package com.vigil.trading.integration.constant

object Endpoint {
    const val TRADING_HISTORY = "/trading-history"
    const val DATA = "/data"
    const val MINE = "/mine"
    const val STATE = "/state"
    const val SYMBOL = "/symbol"
    const val TRANSACTION = "/transaction"
    const val ACCOUNT = "/account"
    const val STRATEGY_CACHE = "/cache"
    const val CONFIG = "/config"

    object Path {
        // State
        const val REFRESH_STATE = "/refresh/{${PathVariableName.ACCOUNT}}/{${PathVariableName.SYMBOL}}"
        const val ACCOUNT_STATE = "/{" + PathVariableName.ACCOUNT + "}"

        // Symbol
        const val SYMBOLS_INFO = "/info/{" + PathVariableName.ACCOUNT + "}"
        const val SYMBOLS = "/{" + PathVariableName.ACCOUNT + "}"
        const val SYMBOLS_COUNT = "/count/{" + PathVariableName.ACCOUNT + "}"
        const val SYMBOL_INFO = "/info/{" + PathVariableName.ACCOUNT + "}/{" + PathVariableName.SYMBOL + "}"

        // History
        const val HISTORY = "/{" + PathVariableName.ACCOUNT + "}"

        // Data
        const val STATS = "/stats"
        const val CANDLES = "/candles"
        const val PRICE = "/price/{" + PathVariableName.SYMBOL + "}"
        const val SIMULATION = "/simulation"

        //Mine
        const val MINE_CANDLES = "/mine-candles"
        const val MINE_NEWS = "/mine-news"
        const val MINE_CONTENT = "/mine-content"
        const val MINE_SENTIMENT = "/mine-sentiment"

        // Transaction
        const val CLOSE_POSITIONS = "/close-positions/{${PathVariableName.ACCOUNT}}"
        const val ALIGN_POSITIONS = "/align-positions"
        const val LEVERAGE = "/leverage/{${PathVariableName.ACCOUNT}}"
        const val ACCOUNT_INFO = "/account-info/{" + PathVariableName.ACCOUNT + "}"
        const val ACCOUNTS_LIST = "/list"
        const val POSITION = "/position/{" + PathVariableName.ACCOUNT + "}/{" + PathVariableName.SYMBOL + "}"
        const val BALANCE_HISTORY = "/balance-history"

        // Engine config
        const val TOGGLE_TRADING = "/toggle-trading/{${PathVariableName.ACCOUNT}}"
        const val TRADING_ENABLED = "/trading-enabled/{${PathVariableName.ACCOUNT}}"

        // Strategy
        const val TRADING_ACTIONS_INFO = "/actions-info"
        const val RECORDS_FOR_SYMBOL = "/records"
    }

    object RequestParamName {
        const val FROM_TIMESTAMP = "fromTimestamp"
        const val TO_TIMESTAMP = "toTimestamp"
        const val TIMESTAMP = "timestamp"
        const val INTERVAL_DURATION = "interval"
        const val LEVERAGE = "leverage"
        const val POSITION_SIDE = "positionSide"
    }

    object PathVariableName {
        const val SYMBOL = "symbol"
        const val SYMBOL_ID = "symbolId"
        const val ACCOUNT = "account"
    }
}
