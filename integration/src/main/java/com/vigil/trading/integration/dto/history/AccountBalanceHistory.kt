package com.vigil.trading.integration.dto.history

import com.vigil.trading.integration.records.TbProfit
import java.math.BigDecimal
import java.time.Instant
import java.util.*

@JvmRecord
data class AccountBalanceHistory(
    val collateral: BigDecimal,

    val maxTrailingDrawdownPercent: BigDecimal,
    val maxTrailingDrawdownTimestamp: Instant?,

    val maxDailyDrawdownPercent: BigDecimal,
    val maxDailyDrawdownTimestamp: Instant?,
    val maxDailyDrawdown80Percentile: BigDecimal,

    val equityHistory: SortedSet<TbProfit>
)
