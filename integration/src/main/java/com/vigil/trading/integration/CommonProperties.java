package com.vigil.trading.integration;

import com.vigil.trading.integration.constant.SbAccount;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;
import java.util.Map;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "common")
public class CommonProperties {

  private Duration tradingInterval;
  private Kafka kafka;

  private Map<SbAccount, Props> account;

  @Getter
  @Setter
  public static class Props {

    private String accountId;
  }

  public Props getProps(final SbAccount account) {
    return this.account.get(account);
  }

  //TODO: move to constants
  @Getter
  @Setter
  public static class Kafka {

    private String candleInfoTopic;

    private String tradingActionsTopic;
    private String transactionServiceGroupId;

    private String instantContextTopic;
    private String accountUpdateTopic;
    private String accountServiceGroupId;

    private String actionsSorterTopic;
    private String actionsSorterGroupId;

    private String processedActionsTopic;
  }
}
