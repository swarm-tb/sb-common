package com.vigil.trading.integration.dto.state

import com.vigil.trading.integration.dto.state.SymbolStateDto
import java.math.BigDecimal
import java.time.Instant

@JvmRecord
data class SbTradingStateDto(
    val symbolsState: List<SymbolStateDto>,
    val shortPositionsCount: Int,
    val longPositionsCount: Int,
    val activePairsCount: Int,
    val currentDailyDrawdownPercent: BigDecimal,
    val drawdownFromStartPercent: BigDecimal,
    val profitPercent: BigDecimal,
    val timestamp: Instant
)
