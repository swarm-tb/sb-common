package com.vigil.trading.integration.retry;

import feign.RetryableException;
import feign.Retryer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.net.ConnectException;
import java.net.UnknownHostException;

@Slf4j
@Component
public class SbRetryer extends Retryer.Default {

  private static final int PERIOD_MILLIS = 10000;
  private static final int MAX_PERIOD_MILLIS = 120000;
  private static final int MAX_ATTEMPTS = 7;

  public SbRetryer() {
    super(PERIOD_MILLIS, MAX_PERIOD_MILLIS, MAX_ATTEMPTS);
  }

  @Override
  public void continueOrPropagate(final RetryableException e) {
    final Throwable cause = e.getCause();
    if (cause instanceof UnknownHostException || cause instanceof ConnectException) {
      log.warn(
          "Feign retry attempt due to {}, Message: {}",
          cause.getClass().getSimpleName(),
          e.getMessage()
      );
      super.continueOrPropagate(e);
    } else {
      throw e;
    }
  }

  @Override
  public Retryer clone() {
    return new SbRetryer();
  }
}
