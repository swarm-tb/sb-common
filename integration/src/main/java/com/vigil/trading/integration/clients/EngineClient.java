package com.vigil.trading.integration.clients;

import com.vigil.trading.integration.constant.Endpoint;
import com.vigil.trading.integration.constant.Endpoint.Path;
import com.vigil.trading.integration.records.SbTradingActionInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.Instant;
import java.util.List;
import java.util.Map;

@FeignClient(name = "${service-name.strategy-service}", path = Endpoint.STRATEGY_CACHE)
public interface EngineClient {

  @GetMapping(Path.TRADING_ACTIONS_INFO)
  Map<Instant, List<SbTradingActionInfo>> getTradingActionsInfo(
      @RequestParam Instant fromTimestamp, @RequestParam(required = false) Instant toTimestamp
  );
}
