package com.vigil.trading.integration.records;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.time.Instant;


public record TbProfit(
    Instant timestamp,
    BigDecimal value
) implements Comparable<TbProfit> {

    @Override
    public int compareTo(@NotNull final TbProfit other) {
        return timestamp.compareTo(other.timestamp);
    }
}
