package com.vigil.trading.integration.clients;

import com.vigil.trading.integration.constant.Endpoint.Path;
import com.vigil.trading.integration.constant.Endpoint.PathVariableName;
import com.vigil.trading.integration.constant.SbAccount;
import com.vigil.trading.integration.dto.history.AccountBalanceHistory;
import com.vigil.trading.integration.records.SbAccountInfo;
import com.vigil.trading.integration.records.SbPosition;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import static com.vigil.trading.integration.constant.Endpoint.ACCOUNT;

@FeignClient(name = "${service-name.transaction-service}")
public interface TransactionClient {

  @GetMapping(ACCOUNT + Path.ACCOUNT_INFO)
  SbAccountInfo getAccountInfo(@PathVariable SbAccount account);

  @GetMapping(ACCOUNT + Path.POSITION)
  SbPosition getPosition(
      @PathVariable(PathVariableName.ACCOUNT) SbAccount account,
      @PathVariable(PathVariableName.SYMBOL) String symbol
  );

  @GetMapping(ACCOUNT + Path.BALANCE_HISTORY)
  AccountBalanceHistory getAccountBalanceHistory();
}
