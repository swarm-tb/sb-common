package com.vigil.trading.integration.models.order;

public final class Constant {

  public static final String ORDERS = "orders";
  public static final String TIMESTAMP_FIELD = "timestamp";
  public static final String META_FIELD = "metaField";
  public static final int SORT_ASC = 1;

}
