package com.vigil.trading.integration.records;

import java.math.BigDecimal;
import java.time.Instant;

public record PriceRecord(
    Instant timestamp,
    BigDecimal price,
    BigDecimal bestBid,
    BigDecimal bestAsk
) {
}
