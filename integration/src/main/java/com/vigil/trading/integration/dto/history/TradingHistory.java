package com.vigil.trading.integration.dto.history;

import com.vigil.trading.integration.records.TbProfit;

import java.math.BigDecimal;
import java.util.Deque;
import java.util.Map;

public record TradingHistory(
    Map<String, Deque<SbPositionHistoryRecord>> symbolPositions,
    BigDecimal totalProfit,
    BigDecimal totalCommission,
    BigDecimal sharpeRatio,
    BigDecimal maxDrawdown,
    Deque<TbProfit> profitHistory,
    AccountBalanceHistory accountBalanceHistory,
    int ordersCount
) {
}
