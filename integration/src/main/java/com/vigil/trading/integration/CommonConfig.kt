package com.vigil.trading.integration

import com.freemanan.starter.grpc.client.EnableGrpcClients
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Configuration

@Configuration
@EnableFeignClients
@EnableGrpcClients
open class CommonConfig
