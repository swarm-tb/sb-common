package com.vigil.trading.integration.constant;

public enum TradingAction {
  BUY,
  SELL,
  CLOSE,
  NEUTRAL
}
