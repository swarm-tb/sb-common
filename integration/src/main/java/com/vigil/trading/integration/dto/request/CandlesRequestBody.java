package com.vigil.trading.integration.dto.request;

import com.vigil.trading.integration.constant.SbAccount;
import com.vigil.trading.integration.records.range.SbTimeRange;

import java.time.Duration;

public record CandlesRequestBody(
    String symbol,
    SbAccount account,
    Duration interval,
    SbTimeRange timeRange
) {
}
