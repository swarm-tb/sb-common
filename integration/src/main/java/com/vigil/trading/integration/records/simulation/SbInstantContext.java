package com.vigil.trading.integration.records.simulation;

import java.time.Instant;
import java.util.Map;

public record SbInstantContext(
    Instant timestamp,
    Map<String, SbSymbolContext> symbolsContext
) {
}
