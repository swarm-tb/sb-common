package com.vigil.trading.integration.records

import com.vigil.trading.integration.constant.TradingAction
import com.vigil.trading.integration.records.SbSymbolInfo
import java.math.BigDecimal
import java.time.Instant

@JvmRecord
data class SbTradingActionInfo(
    val symbolInfo: SbSymbolInfo,
    val timestamp: Instant?,
    val price: BigDecimal?,
    val tradingAction: TradingAction
)
