package com.vigil.trading.integration.records.range;

import java.time.Instant;

public record SbLimitRange(
    Instant fromTimestamp,
    Long limit
) {
}
