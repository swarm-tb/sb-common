package com.vigil.trading.integration.utility

import java.util.concurrent.atomic.AtomicInteger

class ProgressBar(private val total: Int) {
    private val counter = AtomicInteger(0)

    fun increment() {
        counter.incrementAndGet()
        display()
    }

    fun reset() {
        counter.set(0)
    }

    private fun display() {
        val progress = 50
        val percent = counter.toDouble() / total.toDouble()
        val completed = (percent * progress).toInt()
        val remaining = progress - completed
        val bar = "=".repeat(completed) + " ".repeat(remaining)
        print("\rProgress: [$bar] ${String.format("%.2f", percent * 100)}% ($counter/$total)")
        System.out.flush()
    }
}
