package com.vigil.trading.integration.constant.order;

public enum OrderStatus {
  CLOSED,
  OPEN,
  NEW
}
