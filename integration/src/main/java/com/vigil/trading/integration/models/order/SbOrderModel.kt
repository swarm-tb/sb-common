package com.vigil.trading.integration.models.order

import com.vigil.trading.integration.constant.order.OrderSide
import com.vigil.trading.integration.constant.order.OrderStatus
import com.vigil.trading.integration.constant.order.OrderType
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.Instant

@Document(collection = Constant.ORDERS)
@JvmRecord
data class SbOrderModel(
    val timestamp: Instant,  // simulationId
    val metaField: String,
    val symbol: String,
    val status: OrderStatus,
    val side: OrderSide,
    val type: OrderType,
    val price: BigDecimal,
    val size: BigDecimal,
    val cost: BigDecimal,
    val commission: BigDecimal
)
