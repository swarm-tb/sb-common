package com.vigil.trading.integration.dto.request;

import com.vigil.trading.integration.constant.SbAccount;
import com.vigil.trading.integration.records.range.SbTimeRange;

public record StrategyRecordsRequestBody(
    SbAccount account,
    String symbol,
    SbTimeRange timeRange
) {
}
