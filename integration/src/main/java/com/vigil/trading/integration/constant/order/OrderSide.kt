package com.vigil.trading.integration.constant.order

enum class OrderSide {
    BUY,
    SELL,
    CLOSE
}
