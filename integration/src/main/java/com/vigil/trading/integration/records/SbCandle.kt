package com.vigil.trading.integration.records

import java.io.Serializable
import java.math.BigDecimal
import java.time.Instant

@JvmRecord
data class SbCandle(
    val timestamp: Instant,
    val open: BigDecimal,
    val high: BigDecimal,
    val low: BigDecimal,
    val close: BigDecimal,
    val volume: BigDecimal
) : Serializable
