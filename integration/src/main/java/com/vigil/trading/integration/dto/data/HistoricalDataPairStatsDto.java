package com.vigil.trading.integration.dto.data;

import java.math.BigDecimal;

public record HistoricalDataPairStatsDto(
    String symbol,
    BigDecimal amplitude,
    BigDecimal logDelta
) {
}
