package com.vigil.trading.integration.dto.request;

import com.vigil.trading.integration.constant.SbAccount;
import com.vigil.trading.integration.records.range.SbTimeRange;

import java.util.List;

public record SimulationRequestBody(
    SbAccount account,
    List<String> symbols,
    SbTimeRange timeRange
) {
}
