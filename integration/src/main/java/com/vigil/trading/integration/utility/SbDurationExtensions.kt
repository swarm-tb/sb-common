package com.vigil.trading.integration.utility

import java.time.Duration

operator fun Duration.div(divisor: Duration) = this.dividedBy(divisor)
