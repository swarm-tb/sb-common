package com.vigil.trading.integration.records;

import com.vigil.trading.integration.constant.SbAccount;
import lombok.val;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;

public record SbSymbolInfo(
    String symbol,
    SbAccount account,
    String baseAsset,
    String quoteAsset,
    BigDecimal sizeIncrement,
    BigDecimal minSize,
    BigDecimal maxSize,
    BigDecimal contractSize,
    Boolean tradeSession
) {

  public static final String SYMBOL_KEY_SEPARATOR = "-";

  public String getSymbolId() {
    return account + SYMBOL_KEY_SEPARATOR + symbol;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    val that = (SbSymbolInfo) o;

    return new EqualsBuilder().append(getSymbolId(), that.getSymbolId()).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37).append(getSymbolId()).toHashCode();
  }
}
