package com.vigil.trading.integration.records

@JvmRecord
data class SbTimestampInfo(val symbolInfo: SbSymbolInfo, val candle: SbCandle, val news: List<SbNewsArticleData>)
