package com.vigil.trading.integration.dto.state

import com.vigil.trading.integration.constant.SbPositionSide
import java.time.Duration

@JvmRecord
data class SymbolStateDto(
    val symbol: String,
    val minutesDeltaFromLastUpdate: Duration,
    val positionSide: SbPositionSide,
    val tradeSession: Boolean,
    val active: Boolean
)
