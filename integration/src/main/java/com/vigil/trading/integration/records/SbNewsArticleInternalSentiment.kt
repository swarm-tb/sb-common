package com.vigil.trading.integration.records

@JvmRecord
data class SbNewsArticleInternalSentiment(
    val score: Double,
    val reason: String,
    //TODO make required after DB cleanup
    val articleContentId: String?,
)
