package com.vigil.trading.integration.records;

import com.vigil.trading.integration.records.SbTradingActionInfo;

import java.time.Instant;
import java.util.Map;

public record SbTradingActionsSnapshot(
    Instant timestamp,
    Map<String, SbTradingActionInfo> tradingActionsMap
) {
}
