package com.vigil.trading.integration.constant

enum class SbPositionSide {
    LONG,
    SHORT,
    CLOSED
}
