package com.vigil.trading.integration.records

import com.vigil.trading.integration.constant.order.OrderSide
import com.vigil.trading.integration.constant.order.OrderStatus
import com.vigil.trading.integration.constant.order.OrderType
import java.math.BigDecimal
import java.time.Instant

@JvmRecord
data class SbOrder(
    val symbol: String,
    val timestamp: Instant,
    val status: OrderStatus,
    val side: OrderSide,
    val type: OrderType,
    val price: BigDecimal,
    val size: BigDecimal,
    val cost: BigDecimal,
    val commission: BigDecimal
)
