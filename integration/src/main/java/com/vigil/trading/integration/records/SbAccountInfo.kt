package com.vigil.trading.integration.records

import java.math.BigDecimal

@JvmRecord
data class SbAccountInfo(
    val equity: BigDecimal,
    val balance: BigDecimal,
    val currentDailyDrawdownPercent: BigDecimal,
    val drawdownFromStartPercent: BigDecimal,
    val profitPercent: BigDecimal,
    val activeSymbolsCount: Int
)
