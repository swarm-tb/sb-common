package com.vigil.trading.integration.records;

import java.time.Duration;

public record SbTradingConfig(
    Duration tradingRange,
    Duration tradingInterval
) {
}
