package com.vigil.trading.integration.clients;

import com.vigil.trading.integration.constant.Endpoint;
import com.vigil.trading.integration.constant.Endpoint.Path;
import com.vigil.trading.integration.dto.request.CandlesRequestBody;
import com.vigil.trading.integration.records.SbCandle;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "${service-name.data-service}", path = Endpoint.DATA)
public interface DataClient {

  @PostMapping(Path.CANDLES)
  List<SbCandle> getCandles(@RequestBody CandlesRequestBody candlesRequestBody);
}
