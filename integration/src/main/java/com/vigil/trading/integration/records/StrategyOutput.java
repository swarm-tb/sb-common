package com.vigil.trading.integration.records;


public record StrategyOutput(
    String name,
    String value
) {
}
