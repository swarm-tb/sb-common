package com.vigil.trading.integration.records

import java.time.Instant

@JvmRecord
data class SbNewsArticleData(
    val symbol: String,
    val newsUrl: String,
    val timestamp: Instant,
    val sentiment: String,
    val internalSentiment: SbNewsArticleInternalSentiment?,
    val sourceName: String,
    val symbols: List<String>,
)
