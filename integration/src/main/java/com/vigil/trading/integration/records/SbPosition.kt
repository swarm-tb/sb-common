package com.vigil.trading.integration.records

import com.vigil.trading.integration.constant.SbPositionSide
import java.math.BigDecimal

@JvmRecord
data class SbPosition(
    val symbol: String,
    val positionSide: SbPositionSide,
    val volume: BigDecimal,
    val cost: BigDecimal?,
    val openPrice: BigDecimal?,
    val stopLossPrice: BigDecimal?
)
