package com.vigil.trading.integration.records.simulation;

import java.math.BigDecimal;

public record SbSymbolContext(
    BigDecimal price
) {
}
