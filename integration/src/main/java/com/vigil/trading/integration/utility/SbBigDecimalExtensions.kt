package com.vigil.trading.integration.utility

import ch.obermuhlner.math.big.BigDecimalMath
import java.math.BigDecimal
import java.math.MathContext

val HUNDRED: BigDecimal = BigDecimal.valueOf(100)

private val TWO = BigDecimal.valueOf(2)
private val DEFAULT_MATH_CONTEXT = MathContext.DECIMAL64

operator fun BigDecimal.div(divisor: BigDecimal): BigDecimal = this.divide(divisor, DEFAULT_MATH_CONTEXT)

operator fun BigDecimal.div(divisor: Int) = this / divisor.toBigDecimal()

fun BigDecimal.getValueForPercentage(percentage: BigDecimal) = this * percentage / HUNDRED

fun BigDecimal.getDeltaPercentage(changed: BigDecimal): BigDecimal {
    val difference = changed - this
    return difference.getPercentageOf(this.abs())
}

fun BigDecimal.getPercentageOf(total: BigDecimal) = this / total * HUNDRED

fun BigDecimal.half() = this / TWO

fun BigDecimal.logarithm() = BigDecimalMath.log(this, DEFAULT_MATH_CONTEXT)

fun BigDecimal.isZero() = this.signum() == 0

fun BigDecimal.isNegativeOrZero() = this.signum() <= 0

fun BigDecimal.isPositiveOrZero() = this.signum() >= 0

fun BigDecimal.isNegative() = this.signum() < 0

fun BigDecimal.isPositive() = this.signum() > 0
