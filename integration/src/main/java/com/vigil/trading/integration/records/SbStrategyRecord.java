package com.vigil.trading.integration.records;

import com.vigil.trading.integration.constant.TradingAction;

import java.time.Instant;
import java.util.List;

public record SbStrategyRecord(
    Instant timestamp,
    String strategyName,
    TradingAction tradingAction,
    List<StrategyOutput> outputs
) {
}
