package com.vigil.trading.integration.clients

import com.vigil.trading.integration.constant.Endpoint
import com.vigil.trading.integration.constant.Endpoint.PathVariableName.ACCOUNT
import com.vigil.trading.integration.constant.Endpoint.PathVariableName.SYMBOL
import com.vigil.trading.integration.constant.SbAccount
import com.vigil.trading.integration.records.SbSymbolInfo
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable

@FeignClient(name = "\${service-name.symbol-service}", path = Endpoint.SYMBOL)
interface SymbolClient {
    @GetMapping(Endpoint.Path.SYMBOLS_INFO)
    fun getSymbolsInfo(@PathVariable(ACCOUNT) provider: SbAccount): Map<String, SbSymbolInfo>

    @GetMapping(Endpoint.Path.SYMBOLS)
    fun getSymbols(@PathVariable(ACCOUNT) provider: SbAccount): Collection<String>

    @GetMapping(Endpoint.Path.SYMBOL_INFO)
    fun getSymbolInfo(@PathVariable(ACCOUNT) account: SbAccount, @PathVariable(SYMBOL) symbol: String): SbSymbolInfo

    @GetMapping(Endpoint.Path.SYMBOLS_COUNT)
    fun getSymbolsCount(@PathVariable(ACCOUNT) account: SbAccount): Int
}
