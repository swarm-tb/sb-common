package com.vigil.trading.plugin

import io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.credentials.HttpHeaderCredentials
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.testing.Test
import org.gradle.authentication.http.HttpHeaderAuthentication
import org.gradle.kotlin.dsl.*
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.dsl.KotlinVersion.KOTLIN_2_0
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.net.URI

class SbCommonDependenciesPlugin : Plugin<Project> {
    override fun apply(project: Project) {

        project.plugins.apply("kotlin")
        project.plugins.apply("org.springframework.boot")
        project.plugins.apply("io.spring.dependency-management")
        project.plugins.apply("maven-publish")
        project.group = "com.vigil.trading"

        project.repositories {
            mavenLocal()
            mavenCentral()
            maven {
                url = URI("https://gitlab.com/api/v4/groups/59954528/-/packages/maven")
            }
        }

        project.extensions.configure<DependencyManagementExtension> {
            imports {
                mavenBom("org.springframework.cloud:spring-cloud-dependencies:2023.0.2")
            }
        }

        project.tasks.withType<KotlinCompile> {
            compilerOptions {
                freeCompilerArgs.add("-Xjsr305=strict")
                apiVersion.set(KOTLIN_2_0)
                languageVersion.set(KOTLIN_2_0)
                jvmTarget.set(JvmTarget.JVM_17)
            }
        }

        project.dependencies {
            project.dependencies.add("implementation", "org.springframework.boot:spring-boot-starter")
            project.dependencies.add("testImplementation", "org.springframework.boot:spring-boot-starter-test")
            project.dependencies.add(
                "annotationProcessor",
                "org.springframework.boot:spring-boot-configuration-processor"
            )
            project.dependencies.add("developmentOnly", "org.springframework.boot:spring-boot-devtools")

            project.dependencies.add("implementation", "one.util:streamex:0.8.1")
            project.dependencies.add("implementation", "org.apache.commons:commons-collections4:4.4")
            project.dependencies.add("implementation", "org.apache.commons:commons-lang3:3.12.0")

            project.dependencies.add("implementation", "com.google.code.findbugs:jsr305:3.0.2")
            project.dependencies.add(
                "implementation",
                "io.netty:netty-resolver-dns-native-macos:4.1.77.Final:osx-aarch_64"
            )
            project.dependencies.add("implementation", "org.jetbrains:annotations:23.0.0")

            project.dependencies.add("implementation", "org.jetbrains.kotlin:kotlin-reflect:$KOTLIN_VERSION")

            project.dependencies.add("compileOnly", "org.projectlombok:lombok:1.18.28")
            project.dependencies.add("annotationProcessor", "org.projectlombok:lombok:1.18.28")
            project.dependencies.add("testCompileOnly", "org.projectlombok:lombok:1.18.28")
            project.dependencies.add("testAnnotationProcessor", "org.projectlombok:lombok:1.18.28")
        }

        project.tasks.withType<Test> {
            useJUnitPlatform()
        }

        project.extensions.getByType<PublishingExtension>().apply {
            publications {
                create<MavenPublication>("maven") {
                    from(project.components.getByName("kotlin"))
                }
            }
            repositories {
                maven {
                    url =
                        URI("${System.getenv("CI_API_V4_URL")}/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven")
                    credentials(HttpHeaderCredentials::class) {
                        name = "Job-Token"
                        value = System.getenv("CI_JOB_TOKEN")
                    }
                    authentication {
                        create("header", HttpHeaderAuthentication::class.java)
                    }
                }
            }
        }
    }

    companion object {
        const val KOTLIN_VERSION = "2.0.0"
    }
}
