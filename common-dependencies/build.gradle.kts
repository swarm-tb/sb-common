val kotlinVersion = "2.0.0"
val springBootVersion = "3.2.0"
val springBootDependenciesVersion = "1.1.5"

group = "com.vigil.trading"
version = "1.0.1"

plugins {
    `kotlin-dsl`
    `maven-publish`
}

repositories {
    mavenCentral()
}

gradlePlugin {
    plugins {
        create("common-dependencies") {
            id = "com.vigil.trading.common-dependencies"
            implementationClass = "com.vigil.trading.plugin.SbCommonDependenciesPlugin"
        }
    }
}

publishing {
    repositories {
        maven {
            url = uri("${System.getenv("CI_API_V4_URL")}/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                val header by registering(HttpHeaderAuthentication::class)
            }
        }
    }
}

dependencies {
    implementation("io.spring.gradle:dependency-management-plugin:$springBootDependenciesVersion")
    implementation("org.springframework.boot:spring-boot-gradle-plugin:$springBootVersion")
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
}
